FROM alpine:3 as build

# Version pinning for black does not currently working with their versioning schema
# so the following hadolint ignore line is included:
# hadolint ignore=DL3013
RUN apk --no-cache add \
      build-base~=0 \
      gcc~=10 \
      python3~=3 \
      python3-dev~=3 \
      py3-pip~=20 \
  && pip3 install --no-cache-dir black

FROM alpine:3

ENV container docker

COPY --from=build /usr/bin/black /usr/bin/black
COPY --from=build /usr/lib/python3.9/site-packages/ /usr/lib/python3.9/site-packages/

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]
RUN apk --no-cache add \
      python3~=3 \
  && find /usr/lib/ -name '__pycache__' -print0 | xargs -0 -n1 rm -rf \
	&& find /usr/lib/ -name '*.pyc' -print0 | xargs -0 -n1 rm -rf

WORKDIR /work
ENTRYPOINT ["black"]
CMD ["--version"]

ARG BUILD_DATE
ARG REVISION
ARG VERSION

LABEL maintainer="Megabyte Labs <help@megabyte.space"
LABEL org.opencontainers.image.authors="Brian Zalewski <brian@megabyte.space>"
LABEL org.opencontainers.image.created="$BUILD_DATE"
LABEL org.opencontainers.image.description="Node.js files/configurations that support the creation of Dockerfiles"
LABEL org.opencontainers.image.documentation="https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/black/-/blob/master/README.md"
LABEL org.opencontainers.image.licenses="MIT"
LABEL org.opencontainers.image.revision="$REVISION"
LABEL org.opencontainers.image.source="https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/black.git"
LABEL org.opencontainers.image.url="https://megabyte.space"
LABEL org.opencontainers.image.vendor="Megabyte Labs"
LABEL org.opencontainers.image.version="$VERSION"
LABEL space.megabyte.type="ci-pipeline"

